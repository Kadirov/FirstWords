/**
 * Created by ASSET on 17-Aug-17.
 */

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Animated,
    PanResponder,
    Image
} from 'react-native';

class Game3s extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pan: new Animated.ValueXY()
        };
    }

    async saveEntry(key){
        try {
            let scount = await AsyncStorage.getItem(key);
            let icount = Number.parseInt(scount, 10) + 1;
            AsyncStorage.setItem(key, icount.toString());

        } catch (error) {
            // Error saving data
            Alert.alert("Exception in saveEntry: " + error);
        }
    }

    saveEntry(key, value){
        AsyncStorage.setItem(key, value);
    }


    componentWillMount() {
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: () => true,

            // Initially, set the value of x and y to 0 (the center of the screen)
            onPanResponderGrant: (e, gestureState) => {
                this.state.pan.setValue({x: 0, y: 0});
            },

            // When we drag/pan the object, set the delate to the states pan position
            onPanResponderMove: Animated.event([
                null, {dx: this.state.pan.x, dy: this.state.pan.y},
            ]),

            onPanResponderRelease: (e, {vx, vy}) => {
            }
        });
    }


    render() {
        // Destructure the value of pan from the state
        let { pan } = this.state;

        // Calculate the x and y transform from the pan value
        let [translateX, translateY] = [pan.x, pan.y];

        // Calculate the transform property and set it as a value for our style which we add below to the Animated.View component
        let imageStyle = {transform: [{translateX}, {translateY}]};

        return (
            <View style={styles.container}>
                <Animated.View style={imageStyle} {...this._panResponder.panHandlers}>
                    <Image source={require('./img/bee.png')} />
                </Animated.View>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});



// This is important because without exporting PageOne,
// you cannot import it in SampleApp.js
export default Game3s;

