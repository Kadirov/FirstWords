import React from 'react';
import {
    AppRegistry,
    Component,
    StyleSheet,
    Text,
    View,
    Image, // we want to use an image
    PanResponder, // we want to bring in the PanResponder system
    Animated, // we wil be using animated value
    Alert, AsyncStorage
} from 'react-native';
import { StackNavigator } from 'react-navigation';

class Game4 extends React.Component {

    static navigationOptions = {
        title: 'Place the item',
    };

    constructor(props) {
        super(props);

        this.state = {
            pan: new Animated.ValueXY(),
            scale: new Animated.Value(1),
            icount: 2,
        };
    }

    async saveEntry(key){
        try {
            let scount = await AsyncStorage.getItem(key);
            let icount = Number.parseInt(scount, 10) + 1;
            AsyncStorage.setItem(key, icount.toString());

        } catch (error) {
            // Error saving data
            Alert.alert("Exception in saveEntry: " + error);
        }
    }
/*
    saveEntry(key, value){
        AsyncStorage.setItem(key, value);
    }
*/
    componentWillMount() {
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: () => true,

            onPanResponderGrant: (e, gestureState) => {
                /*
                console.log("moving...");
                console.log("x", this.state.pan.x);
                console.log("y", this.state.pan.y._value);
                console.log("evtX",gestureState.moveX);
                console.log("evtY",gestureState.moveY);
                */
                // Set the initial value to the current state
                this.state.pan.setOffset({x: this.state.pan.x._value, y: this.state.pan.y._value});
                this.state.pan.setValue({x: 0, y: 0});
                Animated.spring(
                    this.state.scale,
                    { toValue: 1.1, friction: 3 }
                ).start();
            },

            // When we drag/pan the object, set the delate to the states pan position
            onPanResponderMove: Animated.event([
                null, {dx: this.state.pan.x, dy: this.state.pan.y},
            ]),

            onPanResponderRelease: (e, {vx, vy}, gestureState) => {


                // Flatten the offset to avoid erratic behavior
                this.state.pan.flattenOffset();
                Animated.spring(
                    this.state.scale,
                    { toValue: 1, friction: 3 }
                ).start();

                //console.log("x=" + this.state.pan.x._value + ", y=" + this.state.pan.y._value);
                var locX = this.state.pan.x._value, locY = this.state.pan.y._value;
                if( (locX >=50 && locX <=150) && (locY >=90 && locY<= 160)){
                    //console.log("YES");
                    Alert.alert("Correct", "Good job. You helped the little bird to find the way to its nest.");
                    this.saveEntry("G4OK");
                }
                else {
                    //console.log("NO");
                    Alert.alert("Wrong", "Still close but not yet there." );
                    this.saveEntry("G4NOT");
                }

                //else {this.setState({ icount: this.state.icount + 1 }); Alert.alert(this.state.icount)}
            }
        });
    }

    render() {
        // Destructure the value of pan from the state
        let { pan, scale } = this.state;

        // Calculate the x and y transform from the pan value
        let [translateX, translateY] = [pan.x, pan.y];

        let rotate = '0deg';

        // Calculate the transform property and set it as a value for our style which we add below to the Animated.View component
        let imageStyle = {transform: [{translateX}, {translateY}, {rotate}, {scale}]};

        return (
            <Image source={require('./img/bg4.jpg')} style={styles.backgroundImage}>
                <Text style={styles.welcome}>
                    Help the bird. Move it into the nest.
                </Text>
                <View>
                    <View>
                        <Image source={require('./img/nest.png')} style={styles.nest}/>
                    </View>
                    <View>
                        <Animated.View style={imageStyle} {...this._panResponder.panHandlers}>
                            <Image source={require('./img/bird.png')} />
                        </Animated.View>
                    </View>
                </View>
            </Image>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 18,
        textAlign: 'center',
        margin: 5,
        color: 'white',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
    nest: {
        width: 96,
        height: 96,
        position: 'absolute',
        left:     125,
        top:      175,
    }

});
export default Game4;
