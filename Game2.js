/**
 * Created by ASSET on 16-Aug-17.
 */

import React from 'react';
import { StyleSheet, Text, View, AppRegistry, Image, Button, Alert, TouchableHighlight, TouchableOpacity, Animated,ScrollView, AsyncStorage} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Display from 'react-native-display';

class Game2 extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            objStatus: null,
            isCorrect: false,
            objSelectedItem: '',
        }
    }

    async saveEntry(key){
        try {
            let scount = await AsyncStorage.getItem(key);
            let icount = Number.parseInt(scount, 10) + 1;
            AsyncStorage.setItem(key, icount.toString());

        } catch (error) {
            // Error saving data
            Alert.alert("Exception in saveEntry: " + error);
        }
    }
/*
    saveEntry(key, value){
        AsyncStorage.setItem(key, value);
    }
    */

    toggleDisplay() {
        let toggle = !this.state.isCorrect;
        this.setState({isCorrect: toggle});
    }

    clearSelected() {
        this.state = {
            objStatus: null,
        }
        this.toggleDisplay();
    }

    static navigationOptions = {
        title: 'Odd one out',
    };

    render() {
        return (
            <Image source={require('./img/bg3.jpg')} style={styles.backgroundImage}>
                <ScrollView>
                    <View>
                        <Text style={styles.welcome}>
                            Odd one out
                        </Text>
                    </View>
                    <View style={styles.container_fruits}>
                        <TouchableHighlight
                            onPress={this.oddOneOut.bind(this, 'bird')}>
                            <Image source={require('./img/bird.png')} style={styles.fruits} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.oddOneOut.bind(this, 'butterfly')}>
                            <Image source={require('./img/butterfly.png')} style={styles.fruits} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.oddOneOut.bind(this, 'fish')}>
                            <Image source={require('./img/fish.png')} style={styles.fruits} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.oddOneOut.bind(this, 'bee')}>
                            <Image source={require('./img/bee.png')} style={styles.fruits} />
                        </TouchableHighlight>
                    </View>

                    <View>
                        <Text>Selected image: <Text style={styles.selectedItem}> {this.state.objSelectedItem} {"\n"}</Text></Text>
                    </View>
                    <View><Text> {"\n"} </Text></View>

                    <View style={styles.container_fruits}>
                        <View style={styles.center}>
                            <Display
                                enable={this.state.isCorrect}
                                enterDuration={1000}
                                enter="fadeInUp">
                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                        {this.state.objStatus == 1 ? <Image source={require('./img/animated_star.gif')} /> : <Image source={require('./img/wrong1.png')} />}
                                </View>

                            </Display>
                        </View>
                    </View>
                </ScrollView>
            </Image>
        );
    }

    /**
     *
     */
    oddOneOut(object){
        if(this.state.objStatus != null) this.toggleDisplay();
        //Alert.alert("This image's name is " + title );
        if(object == 'fish'){
            this.setState({objStatus: 1}, () => this.toggleDisplay());
            this.saveEntry("G2OK");

        }
        else {
            this.setState({objStatus: 0}, () => this.toggleDisplay());
            this.saveEntry("G2NOT");
        }

        this.state.objSelectedItem = object;


    };


}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 18,
        textAlign: 'center',
        margin: 5,
        color: '#1194F6',
    },
    selectedItem: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1194F6',
    },
    container_fruits:{
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    fruits:{
        width: 96,
        height: 96,
        margin: 20,
    },
    cont_fruits_text: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    center: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    circle: {
        borderRadius: 50,
        height: 100,
        width: 100,
        margin: 15
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
});



// This is important because without exporting PageOne,
// you cannot import it in SampleApp.js
export default Game2;
