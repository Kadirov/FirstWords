/**
 * Created by ASSET on 15-Jul-17.
 */

import React from 'react';
import { StyleSheet, Text, View, AppRegistry, Image, Button, Alert, TouchableHighlight} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Game1 from './Game1';
import Game2 from './Game2';
import Game3 from './Game3';
import Game3s from './Game3s';
import Game4 from './Game4';
import Game5 from './Game5';
import Analytics from './Analytics';
import AboutPage from './AboutPage';


export default class App extends React.Component {

    static navigationOptions = {
        title: 'Welcome',
    };

    _onPressButton() {
        Alert.alert('You tapped the button!')
    }

    render() {

        const { navigate } = this.props.navigation;

        return (

            <Image source={require('./img/background.jpg')} style={styles.backgroundImage}>

                <View style={styles.container}>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('Games')}
                            title="GAMES"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('Analytics')}
                            title="ANALYTICS"
                            color="#841584"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('AboutPage')}
                            title="About"
                        />
                    </View>
                </View>

            </Image>




        );
    }
}




class GameScreen extends React.Component {
    static navigationOptions = {
        title: 'Choose the game',
    };
    render() {

        const { navigate } = this.props.navigation;

        return (

            <Image source={require('./img/bg4.jpg')} style={styles.backgroundImage}>
                <View style={styles.container}>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('Game1')}
                            title="Match words and pictures"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('Game2')}
                            title="Odd one out"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('Game3')}
                            title="Drag & Drop"
                        />
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button
                            onPress={() => navigate('Game4')}
                            title="Place the item"
                        />
                    </View>
                </View>
            </Image>
            /*
            <TouchableHighlight
                onPress={() => navigate('Game1')}>
                <Image style={styles.button} source={require('./img/logo.png')} />
            </TouchableHighlight>

            <TouchableHighlight onPress={()=>{Alert.alert('You tapped the button!')}}>
                <View>
                    <Icon name="facebook" style={styles.icon}>
                        <Text style={styles.text}>Login with Facebook</Text>
                    </Icon>
                </View>
            </TouchableHighlight>
            */
        );
    }
}

const SimpleApp = StackNavigator({
    Home: { screen: App },
    Games: { screen: GameScreen },
    Game1: { screen: Game1 },
    Game2: { screen: Game2 },
    Game3: { screen: Game3 },
    Game4: { screen: Game4 },
    Analytics: { screen: Analytics },
    AboutPage: { screen: AboutPage },
});

const styles = StyleSheet.create({
    container2: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    buttonContainer: {
        margin: 20
    },
    alternativeLayoutButtonContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});

AppRegistry.registerComponent('FirstWords', () => SimpleApp);

module.exports = SimpleApp;



