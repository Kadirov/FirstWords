/**
 * Created by ASSET on 18-Jul-17.
 */

import React from 'react';
import { StyleSheet, Text, View, AppRegistry, Image, Button, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Animated, AsyncStorage} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Display from 'react-native-display';

class Game1 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedFruit: null,
            selectedFruitText: null,
            matchStatus: null,
            matchStatus2: null,
            imageLink: null,
            isCorrect: false,
        }
    }


    async saveEntry(key){
        try {
            let scount = await AsyncStorage.getItem(key);
            let icount = Number.parseInt(scount, 10) + 1;
            AsyncStorage.setItem(key, icount.toString());

        } catch (error) {
            // Error saving data
            Alert.alert("Exception in saveEntry: " + error);
        }
    }
    /*

    saveEntry(key, value){
        AsyncStorage.setItem(key, value);
    }

*/


    toggleDisplay() {
        let toggle = !this.state.isCorrect;
        this.setState({isCorrect: toggle});
    }

    clearSelected() {
        this.setState({
            selectedFruit: null,
            selectedFruitText: null,
            matchStatus: null,
        });
        this.toggleDisplay();
    }

    static navigationOptions = {
        title: 'Match words and pictures',
    };

/*
    _handlePress() {
        this.props.navigator.push({id: 2,});

    }

    _onPressButton() {
        Alert.alert("This image's name is " );
    }
*/
    render() {
        return (
            /*
             <View style={[styles.container, {backgroundColor: 'green'}]}>
             <Text style={styles.welcome}>Greetings!</Text>
             <TouchableOpacity onPress={this._handlePress}>
             <View style={{paddingVertical: 10, paddingHorizontal: 20,
             backgroundColor: 'black'}}>
             <Text style={styles.welcome}>Go to page two</Text>
             </View>
             </TouchableOpacity>
             </View>

             onPress={() => navigate('Game1')}>
             */
            <Image source={require('./img/bg3.jpg')} style={styles.backgroundImage}>
                <ScrollView>
                    <View>
                        <Text style={styles.welcome}>
                            Match words and pictures
                        </Text>
                    </View>
                    <View style={styles.container_fruits}>
                        <TouchableHighlight
                            onPress={this.getImageTitle.bind(this, 'apple')}>
                            <Image source={require('./img/apple.png')} style={styles.fruits} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.getImageTitle.bind(this, 'cherries')}>
                            <Image source={require('./img/cherries.png')} style={styles.fruits} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.getImageTitle.bind(this, 'grapes')}>
                            <Image source={require('./img/grapes.png')} style={styles.fruits} />
                        </TouchableHighlight>
                        <TouchableHighlight
                            onPress={this.getImageTitle.bind(this, 'pear')}>
                            <Image source={require('./img/pear.png')} style={styles.fruits} />
                        </TouchableHighlight>
                    </View>

                    <View>
                        <Text>Selected image: <Text style={styles.selectedItem}> {this.state.selectedFruit}</Text></Text>
                    </View>

                    <View>
                        <View style={styles.cont_fruits_text}>
                            <Button onPress={this.getTextButtonTitle.bind(this, 'grapes')} title="GRAPES"/>
                            <Button onPress={this.getTextButtonTitle.bind(this, 'cherries')} title="CHERRIES" />
                            <Button onPress={this.getTextButtonTitle.bind(this, 'apple')} title="APPLE" />
                            <Button onPress={this.getTextButtonTitle.bind(this, 'pear')} title="PEAR" />
                        </View>

                    </View>

                    <View>
                        <Text>Selected text: <Text style={styles.selectedItem}> {this.state.selectedFruitText}</Text></Text>
                    </View>
                    <View style={styles.container_fruits}>
                        <View style={styles.center}>
                            <Display
                                enable={this.state.isCorrect}
                                enterDuration={1000}
                                enter="fadeInUp">
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {this.state.matchStatus == 'OK!'? <Image source={require('./img/animated_star.gif')} /> : <Image source={require('./img/wrong1.png')} />}
                                </View>
                            </Display>
                        </View>
                    </View>
                </ScrollView>
            </Image>
        );
    }

    /**
     * gets the title of the clicked image, displays it on status bar
     */
    getImageTitle(img_title){
        //Alert.alert("This image's name is " + title );
        if(this.state.matchStatus != null ){
            this.clearSelected();
        }
        this.setState({selectedFruit: img_title}, () => this.checkTappedItems());
    };

    /**
     * gets the text of the clicked button, displays it on status bar
     * @param text
     */
    getTextButtonTitle(text){
        if(this.state.matchStatus != null ){
            this.clearSelected();
        }
        this.setState({selectedFruitText: text} ,() => this.checkTappedItems());
    }


    checkTappedItems(){

        this.setState({matchStatus2: 'fruit=' + this.state.selectedFruit + '&&&&text=' + this.state.selectedFruitText});
        if (this.state.selectedFruitText != null && this.state.selectedFruit!= null ){

            if(this.state.selectedFruit == this.state.selectedFruitText)
            {
                this.setState({matchStatus : 'OK!'});
                //persist attempt in DB: success
                this.saveEntry("G1OK");
            }
                else {
                this.setState({matchStatus : 'FALSE!'});
                //persist attempt in DB: fail
                this.saveEntry("G1NOT");
            }
            this.toggleDisplay();

        }

    }
}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 18,
        textAlign: 'center',
        margin: 5,
        color: '#1194F6',
    },
    selectedItem: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#1194F6',
    },
    container_fruits:{
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    fruits:{
        width: 96,
        height: 96,
        margin: 20
    },
    cont_fruits_text: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    center: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    circle: {
        borderRadius: 50,
        height: 100,
        width: 100,
        margin: 15
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
});



// This is important because without exporting PageOne,
// you cannot import it in SampleApp.js
export default Game1;
